import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Cinema cinema = new Cinema("Prestige", 9, 20);
//        cinema.getSeats();
//        if(cinema.reserveSeat("B11")) {
//            System.out.println("Please complete the payment");
//        } else {
//            System.out.println("Sorry, this seat is taken");
//        }
//        if(cinema.reserveSeat("B11")) {
//            System.out.println("Please complete the payment");
//        } else {
//            System.out.println("Sorry, this seat is taken");
//        }

        List<Cinema.Seat> listSortedAccordingToPrice = new ArrayList<>(cinema.getSeats());
        listSortedAccordingToPrice.add(cinema.new Seat("B00", 9.00));
        listSortedAccordingToPrice.add(cinema.new Seat("A00", 9.00));
        /*
        * Here we are using the Comparator implementation we provide in Cinema class to the sort method of the
        * Collections class. This is an example case where instead of implementing the Comparable interface
        * in our Seat class we are using a comparator implementation. We could have added the Comparator implementation inside
        * the Seat class as well. Actually that could have been better :)
        * */
        Collections.sort(listSortedAccordingToPrice, Cinema.PRICE_ORDER);
        /*
        * seats will be listed in price order. other than listing according to the price, items will be listed in the order
        * they were added to the list. pay attention that B00 is listed before A00. there will not be any sorting according to
        * seat numbers. this provides efficiency. items will not be swapped unnecessarily by their set numbers, they will be sorted
        * purely by their prices.
        * we can add as many comparators as we like. we could have added another comparator to make the sorting the other
        * way around.
        * another thing is, you don't need to make your comparator implementations static. if you don't need an instance, that would
        * be an easier and quicker way of accessing a comparator if you define then as static though.
        * */
        printList(listSortedAccordingToPrice);
    }

    public static void printList(List<Cinema.Seat> list){
        for(Cinema.Seat seat : list){
            System.out.println(" " + seat.getSeatNumber() + " $" + seat.getPrice());
        }
        System.out.println("XXXXXXXXXXXXXXXX");
    }
}