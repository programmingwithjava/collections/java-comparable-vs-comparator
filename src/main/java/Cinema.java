import java.util.*;


public class Cinema {
    private final String cinemaName;
    /*
    * it may look like we are instantiating the Comparator interface but what we are doing is actually
    * creating an anonymous inner class implementing Comparator interface and it is providing an implementation
    * for the compare method.
    * */
    static final Comparator<Seat> PRICE_ORDER = new Comparator<Cinema.Seat>() {
		
		@Override
		public int compare(Seat s1, Seat s2) {
			if(s1.getPrice() < s2.getPrice()) {
			    return -1;
			} else if(s1.getPrice() > s2.getPrice()) {
					return 1;
            } else {
					return 0;
            }
			
		}
	};
    /*
    * at the top level of the Collections framework is the Collections class.
    * this class exposes static methods that operate on collections.
    *
    * core elements of the Collections framework are interfaces(Set, List Queue, Deque).
    * another core elements are the implementations like ArrayList, LinkedList etc.
    * another core component of the framework are polymorphic algorithms that work on collections objects (objects that implement
    * Collection interface).
    *
    * we can change the ArrayList with an LinkedList and the code would still work.
    * private List<Seat> seats = new LinkedList<>();
    *
    * then we can change List to Collection, our code would still work.
    * private Collection<Seat> seats = new LinkedList<>();
    *
    * we we define "seats" to be of type Collection, we can implement the list of seats in our cinema using any concrete class
    * that implements one of the interfaces that extends Collection (Set, List Queue, Deque). we can also use HashSet or LinkedHashSet
    * that implements "Set". any of the interfaces that are 1 level down, we may not use directly(SortedSet level) when "seats" is defined as "Collection".
    *
    * private Collection<Seat> seats = new HashSet<>();
    * private Collection<Seat> seats = new HashSet<>();
    *
    * so we can use different data structure if that structure provide benefits for a particular task.
    *
    * we need to be careful about the level of the implementation classes when we replace one with the other.
    * consider the hierarchy diagram in the wiki page.
    *
    * Let say we have this:
    * private Collection<Seat> seats = new ArrayList<>();
    *
    * when we define "seats" as type Collection we can use any implementation classes that implement Set, List, Queue, Deque.
    * any implementation classes that are more down below below can't be used straight away.
    *
    * for example if we try to use TreeSet(which is 2 levels down - see the diagram on wiki page) that implements NavigableSet
    * which extends SortedSet and it has an additional requirement.
    * the elements it contains must be comparable. if our seat class is not comparable we will encounter a ClassCastException:
    * private Collection<Seat> seats = new TreeSet<>();
    *
    * as we move down the hierarchy the interface will become more specialized.
    *
    * */
    private List<Seat> seats = new ArrayList<>();

    public Cinema(String cinemaName, int numRows, int seatsPerRow) {
        this.cinemaName = cinemaName;

        int lastRow = 'A' + (numRows -1);
        for (char row = 'A'; row <= lastRow; row++) {
            for(int seatNum = 1; seatNum <= seatsPerRow; seatNum++) {
            	double price = 10.00;
            	if((row<'D') && (seatNum >= 4 && seatNum <= 9)) {
            		price = 12.00;
            	}else if((row>'F')||(seatNum<4 || seatNum>9)) {
            		price = 8.00;
            	}
            	
                Seat seat = new Seat(row + String.format("%02d", seatNum), price);
                seats.add(seat);
            }
        }
    }

    public String getCinemaName() {
        return cinemaName;
    }

    public boolean reserveSeat(String seatNumber) {
//        Seat requestedSeat = null;
//        for(Seat seat : seats) {
//            /*
//            * see how many checks mode before finding relevant seat.
//            * this is a very inefficient "brute force" search. it scans every element till we find the
//            * one we are interested in.
//            * Collections class provides a binary search method that performs much better.
//            * for that to work we need to implement Comparable interface in Seat class.
//            * */
//            System.out.print(".");
//            if(seat.getSeatNumber().equals(seatNumber)) {
//                requestedSeat = seat;
//                break;
//            }
//        }
//        if(requestedSeat == null) {
//            System.out.println("There is no seat " + seatNumber);
//            return false;
//        }
//
//        return requestedSeat.reserve();

        /*
        * with binarySearch
        * this is the fastest way to find an item in a sorted list.
        * if you invoke the reserveSeat method from the main class it won't work. you need to implement
        * Comparable interface in the Seat class (commented out below).
        * */
        Seat seat = new Seat(seatNumber, 0);
        int foundSeat = Collections.binarySearch(seats, seat, null);
        if(foundSeat >= 0){
            return seats.get(foundSeat).reserve();
        }else{
            System.out.println("There is no set " + seatNumber);
            return false;
        }
    }

    // for testing
//    public void getSeats() {
//        for(Seat seat : seats) {
//            System.out.println(seat.getSeatNumber());
//        }
//    }
    
    public Collection<Seat> getSeats() {
    	return seats;
    }

    /*
    * after implementing Comparable interface, we can use binarySearch of the Collections class
    * in the reserveSeat method.
    * 
    * COMPARABLE vs COMPARATOR
    * 
    * it is also the implementation of the Comparable interface that makes it possible to use Collections methods like sort, reverse etc..
    * we just need to implement (override) the compareTo method.
    * another way to use the sort method is to pass a Comparator to it.  Comparator interface defines a single method "compare".
    * An object of type Comparator is created with a compare method and this method will be used to compare the relevant objects. unlike with 
    * Comparable, types to be compared (Seat objects in this case) do not need to implement this Comparator interface.
    *
    * Class below has been made public for demonstration purposes. In most of the cases you will want to keep your inner classes private.
    * */
    public class Seat {//implements Comparable<Seat>
        private final String seatNumber;
        private boolean reserved = false;
        private double price;

        public Seat(String seatNumber, double price) {
            this.seatNumber = seatNumber;
            this.price = price;
        }

//        @Override
//        public int compareTo(Seat seat) {
//            return this.seatNumber.compareToIgnoreCase(seat.getSeatNumber());
//        }

        public boolean reserve() {
            if(!this.reserved) {
                this.reserved = true;
                System.out.println("Seat " + seatNumber + " reserved");
                return true;
            } else {
                return false;
            }
        }

        public boolean cancel() {
            if(this.reserved) {
                this.reserved = false;
                System.out.println("Reservation of seat " + seatNumber + " cancelled");
                return true;
            } else {
                return false;
            }
        }

        public String getSeatNumber() {
            return seatNumber;
        }

		/**
		 * @return the price
		 */
		public double getPrice() {
			return price;
		}
    }
}
